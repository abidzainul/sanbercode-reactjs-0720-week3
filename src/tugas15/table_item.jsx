import React from 'react';

const TableItem = (props) => {

    const onEdit = (id, e) => {
        e.preventDefault();
        props.onEdit(id, e)
    }
    
    const onDelete = (id, e) => {
        e.preventDefault();
        props.onDelete(id, e)
    }

    return (
        <tr>
            <td>{props.data.name}</td>
            <td>{props.data.price}</td>
            <td>{props.data.weight}</td>
            <td>
                <button onClick={(e) => onEdit(props.data.id, e)}>Edit</button>
                <button onClick={(e) => onDelete(props.data.id, e)}>Delete</button>
            </td>
        </tr>
    )
}

export default TableItem