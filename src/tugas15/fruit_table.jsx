import React, { useState, useEffect, useContext } from 'react';
import TableItem from './table_item';
import client from './api_services';
import { FruitContext } from './fruit_provider';

const TableTitle = () => {
    return(
        <thead>
            <tr>
                <td style={{width: 300}}>name</td>
                <td style={{width: 150}}>price</td>
                <td style={{width: 150}}>weight</td>
                <td style={{width: 60}}>Action</td>
            </tr>
        </thead>
    )
}

const FruitTable = () => {
    const { fruits, fruit, update } = useContext(FruitContext);
    const [listBuah, setListBuah] = fruits
    const [buah, setBuah] = fruit
    const [isUpdate, setIsUpdate] = update

    useEffect(() => {
        loadAll();
      }, []);

    const loadAll = () => {
        client.getAll()
        .then(res => {
            console.log(res.data);
            setListBuah(res.data);
        })
    }
    
    const onEdit = (id, event) => {
        console.log(id)
        client.get(id)
            .then(res => {
                console.log("edit")
                setBuah(res.data)
            }).catch(e => {
                console.log(e)
            })

        setIsUpdate(true)
    }
    
    const onDelete = (id, event) => {
        event.preventDefault()
        console.log(id)
        client.remove(id)
            .then(res => {
                console.log("delete")
                loadAll()
            }).catch(e => {
                console.log(e)
            })
        
        setIsUpdate(false)
    }
    
    return (
        <div>
            <h1>Table price Buah</h1>
            <table>
                <TableTitle />
                <tbody>
                    {
                        listBuah.map( (buah, i) => 
                            <TableItem key={i} 
                            data={buah} 
                            onEdit={onEdit} 
                            onDelete={onDelete} 
                            index={i}/>
                        )
                    }
                </tbody>
            </table>
        </div>
    )
}

export default FruitTable