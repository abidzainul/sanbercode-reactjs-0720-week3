import React, { useState, useEffect, createContext } from 'react';

export const FruitContext = createContext();

export const FruitProvider = props => {
    const [listBuah, setListBuah] = useState([]);
    const [buah, setBuah] = useState ({
        id: null,
        name: "",
        price: 0,
        weight: ""
    })
    const [isUpdate, setIsUpdate] = useState(false)
  
    return (
      <FruitContext.Provider 
        value={{
          fruits: [listBuah, setListBuah], 
          fruit: [buah, setBuah], 
          update: [isUpdate, setIsUpdate]}}>
            
        {props.children}
      </FruitContext.Provider>
    );
  };