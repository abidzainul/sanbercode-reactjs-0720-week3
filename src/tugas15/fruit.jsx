import React from "react"
import { FruitProvider } from "./fruit_provider"
import FruitTable from "./fruit_table"
import FruitForm from "./fruit_form"

const Fruit = () => {
  return(
    <FruitProvider>
        <FruitForm/>
        <FruitTable/>
    </FruitProvider>
  )
}

export default Fruit