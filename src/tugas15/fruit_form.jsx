import React, { useState, useEffect, useContext } from 'react';
import client from './api_services';
import { FruitContext } from './fruit_provider';

const FruitForm = () => {
    const { fruits, fruit, update } = useContext(FruitContext);
    const [listBuah, setListBuah] = fruits
    const [buah, setBuah] = fruit
    const [isUpdate, setIsUpdate] = update

    const loadAll = () => {
        client.getAll()
        .then(res => {
            console.log(res.data);
            setListBuah(res.data);
        })
    }

    const handleChange = (event) => {
        const value = event.target.value;
        setBuah({
            ...buah,
            [event.target.name]: value
        });
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(buah)

        if(isUpdate){
            client.update(buah.id, buah)
                .then(res => {
                    console.log(`insert: ${res}`)
                    reset()
                }).catch(e => {
                    console.log(`error: ${e}`)
                })
        }else {
            client.insert(buah)
                .then(res => {
                    console.log(`update: ${res}`)
                    reset()
                }).catch(e => {
                    console.log(`error: ${e}`)
                })
        }
    }

    const reset = () => {
        setBuah({
            name: "",
            price: 0,
            weight: ""
        })

        setIsUpdate(false)
        loadAll()
    }
    
    return (
        <div>
            <h1>Form Input</h1>
            <form onSubmit={handleSubmit}>
                <div className="row">
                    <label>
                        name Buah
                    </label>          
                    <input type="text" 
                        name="name"
                        value={buah.name}
                        onChange={handleChange}/>
                </div>
                <div className="row">
                    <label>
                        price
                    </label>          
                    <input type="text" 
                        name="price"
                        value={buah.price}
                        onChange={handleChange}/>
                </div>
                <div className="row">
                    <label>
                        weight
                    </label>          
                    <input type="text" 
                        name="weight"
                        value={buah.weight}
                        onChange={handleChange}/>
                </div>
                <input type="submit" value="Submit" />
            </form>
        </div>
    )
}

export default FruitForm