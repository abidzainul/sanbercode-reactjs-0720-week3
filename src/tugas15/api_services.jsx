import http from './axios_config';

const getAll = () => {
    return http.get(`/fruits`)
}

const get = (id) => {
    return http.get(`/fruits/${id}`)
}

const insert = (data) => {
    return http.post(`/fruits`, data)
}

const update = (id, data) => {
    return http.put(`/fruits/${id}`, data)
}

const remove = (id) => {
    return http.delete(`/fruits/${id}`)
}
  
  export default {
    getAll,
    get,
    insert,
    update,
    remove
  };