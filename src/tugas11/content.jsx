import React from 'react';
import TableItem from './table_item';

class TableTitle extends React.Component {
    render(){
        return(
            <thead>
                <tr>
                    <td style={{width: 300}}>Nama</td>
                    <td style={{width: 150}}>Harga</td>
                    <td style={{width: 150}}>Berat</td>
                </tr>
            </thead>
        )
    }
}

class Content extends React.Component {
    constructor(){
        super();
        this.state = {
            listBuah: [
                {
                    "nama": "Semangka",
                    "harga": 10000,
                    "berat": "1 kg"
                },
                {
                    "nama": "Anggur",
                    "harga": 40000,
                    "berat": "0.5 kg"
                },
                {
                    "nama": "Strawberry",
                    "harga": 30000,
                    "berat": "0.4 kg"
                },
                {
                    "nama": "Jeruk",
                    "harga": 30000,
                    "berat": "1 kg"
                },
                {
                    "nama": "Mangga",
                    "harga": 30000,
                    "berat": "0.5 kg"
                },
            ]
        }
    }
    render(){
        return (
            <table>
                <TableTitle />
                <tbody>
                    {
                        this.state.listBuah.map( (buah, i) => 
                            <TableItem key={i} data={buah} />
                        )
                    }
                </tbody>
            </table>
        )
    }
}

export default Content