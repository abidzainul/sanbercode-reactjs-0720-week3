import React from 'react';

class TableItem extends React.Component {
    render() {
        return (
        <tr>
            <td>{this.props.data.nama}</td>
            <td>{this.props.data.harga}</td>
            <td>{this.props.data.berat}</td>
        </tr>
        )
    }
}

export default TableItem