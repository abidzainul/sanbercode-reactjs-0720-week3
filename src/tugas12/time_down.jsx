import React from 'react';

class TimeDown extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            timer: 110,
            curTime: null,
            mid: "AM",
            showTime: true,
        }
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentDidUpdate(props, state) {
        console.log(state.timer)
        if(state.timer === 0){
            this.setState({
                showTime: false,
            })
            clearInterval(this.timerID);
        }
    }

    componentWillUnmount(){
        clearInterval(this.timerID);
    }

    tick(){
        const now = new Date()
        let hours = now.getHours()
        let minutes = now.getMinutes()
        let seconds = now.getSeconds()
        let mid = "AM"
        
        if(hours === 0){
            hours = 12;
        } else if(hours > 12){
            hours = hours % 12
            mid = 'PM'
        }

        this.setState({
            curTime: `${hours}:${minutes}:${seconds} ${mid}`,
            mid: mid,
            timer: this.state.timer - 1 
        })
    }

    render() {
        var time = this.state.showTime ? <ViewTime data={this.state} /> : '';
        return(
            <div>
                {time}
            </div>
        )
    }
}

class ViewTime extends React.Component {

    componentWillUnmount(){
        console.log("componentWillUnmount")
    }

    render(){
        return(
            <div className="timedown">
                <h1>Sekarang Jam: {this.props.data.curTime}</h1>
                <h1>Hitung Mundur: {this.props.data.timer}</h1>
            </div>
        )
    }
}

export default TimeDown