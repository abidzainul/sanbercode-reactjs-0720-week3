import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Tugas11 from './tugas11/content';
import Tugas12 from './tugas12/content';
import Tugas13 from './tugas13/content';
import Tugas14 from './tugas14/content';
import Tugas15 from './tugas15/fruit';

function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/tugas11">Tugas 11</Link>
            </li>
            <li>
              <Link to="/tugas12">Tugas 12</Link>
            </li>
            <li>
              <Link to="/tugas13">Tugas 13</Link>
            </li>
            <li>
              <Link to="/tugas14">Tugas 14</Link>
            </li>
            <li>
              <Link to="/tugas15">Tugas 15</Link>
            </li>
          </ul>
        </nav>
        
        <Switch>
          <Route path="/tugas11">
            <Tugas11 />
          </Route>
          <Route path="/tugas12">
            <Tugas12 />
          </Route>
          <Route path="/tugas13">
            <Tugas13 />
          </Route>
          <Route path="/tugas14">
            <Tugas14 />
          </Route>
          <Route path="/tugas15">
            <Tugas15 />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
