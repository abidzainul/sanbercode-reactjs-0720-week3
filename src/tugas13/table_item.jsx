import React from 'react';

class TableItem extends React.Component {
    constructor(props){
        super(props)
    }
    
    onEdit(index, e){
        e.preventDefault();
        this.props.onEdit(index, e)
    }
    
    onDelete(index, e){
        e.preventDefault();
        this.props.onDelete(index, e)
    }

    render() {
        return (
        <tr>
            <td>{this.props.data.nama}</td>
            <td>{this.props.data.harga}</td>
            <td>{this.props.data.berat}</td>
            <td>
                <button onClick={(e) => this.onEdit(this.props.index, e)}>Edit</button>
                <button onClick={(e) => this.onDelete(this.props.index, e)}>Delete</button>
            </td>
        </tr>
        )
    }
}

export default TableItem