import React from 'react';
import TableItem from './table_item';

class TableTitle extends React.Component {
    render(){
        return(
            <thead>
                <tr>
                    <td style={{width: 300}}>Nama</td>
                    <td style={{width: 150}}>Harga</td>
                    <td style={{width: 150}}>Berat</td>
                    <td style={{width: 60}}>Action</td>
                </tr>
            </thead>
        )
    }
}

class Content extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            listBuah: [
                {
                    "nama": "Semangka",
                    "harga": 10000,
                    "berat": "1 kg"
                },
                {
                    "nama": "Anggur",
                    "harga": 40000,
                    "berat": "0.5 kg"
                },
                {
                    "nama": "Strawberry",
                    "harga": 30000,
                    "berat": "0.4 kg"
                },
                {
                    "nama": "Jeruk",
                    "harga": 30000,
                    "berat": "1 kg"
                },
                {
                    "nama": "Mangga",
                    "harga": 30000,
                    "berat": "0.5 kg"
                },
            ],
            namaBuah: "",
            harga: 0,
            berat: "",
            index: -1
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }
    
    handleChange(event){
        const value = event.target.value;
        this.setState({
            [event.target.name]: value
        });
    }
    
    onEdit(index, event){
        console.log(index)
        this.setState({
            namaBuah: this.state.listBuah[index].nama,
            harga: this.state.listBuah[index].harga,
            berat: this.state.listBuah[index].berat,
            index: index
        });

    }
    
    onDelete(index, event){
        event.preventDefault()
        console.log(this.state.listBuah[index].nama)
        let arr = [...this.state.listBuah];
        
        arr.splice(index, 1);
        this.setState({listBuah: arr});
    }

    handleSubmit(event){
        event.preventDefault()

        let buah = {
            "nama": this.state.namaBuah,
            "harga": this.state.harga,
            "berat": this.state.berat
        }

        if(this.state.index != -1){
            let arr = [...this.state.listBuah];
            
            arr.splice(this.state.index, 1, buah);

            this.setState({
                listBuah: arr,
                namaBuah: "",
                harga: 0,
                berat: "",
                index: -1
            })
            return;
        }

        this.setState({
            listBuah: [...this.state.listBuah, buah],
            namaBuah: "",
            harga: 0,
            berat: "",
            index: -1
        })
    }
    
    render(){
        return (
            <div>
                
                <h1>Form Input</h1>
                <form onSubmit={this.handleSubmit}>
                    <div className="row">
                        <label>
                            Nama Buah
                        </label>          
                        <input type="text" 
                            name="namaBuah"
                            value={this.state.namaBuah}
                            onChange={this.handleChange}/>
                    </div>
                    <div className="row">
                        <label>
                            Harga
                        </label>          
                        <input type="text" 
                            name="harga"
                            value={this.state.harga}
                            onChange={this.handleChange}/>
                    </div>
                    <div className="row">
                        <label>
                            Berat
                        </label>          
                        <input type="text" 
                            name="berat"
                            value={this.state.berat}
                            onChange={this.handleChange}/>
                    </div>
                    <input type="submit" value="Submit" />
                </form>
                
                <h1>Table Harga Buah</h1>
                <table>
                    <TableTitle />
                    <tbody>
                        {
                            this.state.listBuah.map( (buah, i) => 
                                <TableItem key={i} 
                                data={buah} 
                                onEdit={this.onEdit} 
                                onDelete={this.onDelete} 
                                index={i}/>
                            )
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Content