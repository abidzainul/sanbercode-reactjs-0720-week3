import React from 'react';
import TableItem from './table_item';

class TableTitle extends React.Component {
    render(){
        return(
            <thead>
                <tr>
                    <td style={{width: 300}}>Nama</td>
                    <td style={{width: 150}}>Harga</td>
                    <td style={{width: 150}}>Berat</td>
                </tr>
            </thead>
        )
    }
}

class TableView extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        return (
            <table>
                <TableTitle />
                <tbody>
                    {
                        this.props.data.map( (buah, i) => 
                            <TableItem key={i} data={buah} index={i}/>
                        )
                    }
                </tbody>
            </table>
        )
    }
}

export default TableView