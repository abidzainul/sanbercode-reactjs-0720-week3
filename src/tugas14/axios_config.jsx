import axios from "axios";

export default axios.create({
  baseURL: "http://backendexample.sanbercloud.com/api",
  headers: {
    "Content-type": "application/json"
  }
});