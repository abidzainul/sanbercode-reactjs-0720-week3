import React, { useState, useEffect } from 'react';
import TableItem from './table_item';
import client from './api_services';

const TableTitle = () => {
    return(
        <thead>
            <tr>
                <td style={{width: 300}}>name</td>
                <td style={{width: 150}}>price</td>
                <td style={{width: 150}}>weight</td>
                <td style={{width: 60}}>Action</td>
            </tr>
        </thead>
    )
}

const Content = () => {
    const [listBuah, setListBuah] = useState([])
    const [buah, setBuah] = useState ({
        id: null,
        name: "",
        price: 0,
        weight: ""
    })
    const [isUpdate, setIsUpdate] = useState(false)

    useEffect(() => {
        loadAll();
      }, []);

    const loadAll = () => {
        client.getAll()
        .then(res => {
            console.log(res.data);
            setListBuah(res.data);
        })
    }

    const handleChange = (event) => {
        const value = event.target.value;
        setBuah({
            ...buah,
            [event.target.name]: value
        });
    }
    
    const onEdit = (id, event) => {
        console.log(id)
        client.get(id)
            .then(res => {
                console.log("edit")
                setBuah(res.data)
            }).catch(e => {
                console.log(e)
            })

        setIsUpdate(true)
    }
    
    const onDelete = (id, event) => {
        event.preventDefault()
        console.log(id)
        client.remove(id)
            .then(res => {
                console.log("delete")
                loadAll()
            }).catch(e => {
                console.log(e)
            })
        
        setIsUpdate(false)
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(buah)

        if(isUpdate){
            client.update(buah.id, buah)
                .then(res => {
                    console.log(`insert: ${res}`)
                    reset()
                }).catch(e => {
                    console.log(`error: ${e}`)
                })
        }else {
            client.insert(buah)
                .then(res => {
                    console.log(`update: ${res}`)
                    reset()
                }).catch(e => {
                    console.log(`error: ${e}`)
                })
        }
    }

    const reset = () => {
        setBuah({
            name: "",
            price: 0,
            weight: ""
        })

        setIsUpdate(false)
        loadAll()
    }
    
    return (
        <div>
            
            <h1>Form Input</h1>
            <form onSubmit={handleSubmit}>
                <div className="row">
                    <label>
                        name Buah
                    </label>          
                    <input type="text" 
                        name="name"
                        value={buah.name}
                        onChange={handleChange}/>
                </div>
                <div className="row">
                    <label>
                        price
                    </label>          
                    <input type="text" 
                        name="price"
                        value={buah.price}
                        onChange={handleChange}/>
                </div>
                <div className="row">
                    <label>
                        weight
                    </label>          
                    <input type="text" 
                        name="weight"
                        value={buah.weight}
                        onChange={handleChange}/>
                </div>
                <input type="submit" value="Submit" />
            </form>
            
            <h1>Table price Buah</h1>
            <table>
                <TableTitle />
                <tbody>
                    {
                        listBuah.map( (buah, i) => 
                            <TableItem key={i} 
                            data={buah} 
                            onEdit={onEdit} 
                            onDelete={onDelete} 
                            index={i}/>
                        )
                    }
                </tbody>
            </table>
        </div>
    )
}

export default Content